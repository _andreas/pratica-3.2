/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andreas
 */
public class Pratica32 {

    public static void main(String[] args) {
        System.out.println(densidade(-1, 67, 3));
    }

    public static double densidade(double x, double media,
            double desvio) {
        double d = ((1/(Math.sqrt(2.0*Math.PI)*desvio))* Math.exp(-((1.0/2.0)*Math.pow((x - media)/desvio, 2.0))));
        return d;
    }
}
